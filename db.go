package main

import (
	"database/sql"
	"log"
	"strconv"
)

func getNgcObjects(db *sql.DB, limit int) []NgcObject {
	if limit == 0 {
		limit = 10
	}
	query := "SELECT * FROM `ngcobjects` LIMIT " + strconv.Itoa(limit)
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var ngcObjects []NgcObject
	for rows.Next() {
		p := NgcObject{}
		err := rows.Scan(&p.Id, &p.Number, &p.Imagepath, &p.Description)
		if err != nil {
			log.Println(err)
			continue
		}
		ngcObjects = append(ngcObjects, p)
	}
	return ngcObjects
}
func findNgcObjectByNumber(db *sql.DB, number string) NgcObject {
	query := "SELECT * FROM `ngcobjects` WHERE number = " + number + " LIMIT 1"

	var ngcObject NgcObject
	row := db.QueryRow(query)
	err := row.Scan(&ngcObject.Id, &ngcObject.Number, &ngcObject.Imagepath,
		&ngcObject.Description)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	return ngcObject
}

func storeNgcObject(db *sql.DB, number string, imagepath string, description string) (id int64, err error) {
	result, err := db.Exec("insert into `ngcobjects` (number, imagepath, description) values ($1, $2, $3)",
		number, imagepath, description)
	if err != nil {
		log.Fatal(err)
	}
	return result.LastInsertId()
}

func deleteNgcObject(db *sql.DB, id int64) (rowID int64, err error) {
	result, err := db.Exec("delete from `ngcobjects` where id = $1", id)
	if err != nil {
		log.Fatal(err)
	}
	return result.RowsAffected()
}

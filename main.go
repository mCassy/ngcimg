package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/PuerkitoBio/goquery"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

var (
	conf Config
)

func getImageLink(element *goquery.Selection) (response string, ok bool) {
	href, exists := element.Attr("href")
	if exists {
		return conf.Basic.Baseurl + href, true
	}
	return "", false
}

func getNGCDescriptionLink(element *goquery.Selection) (response string, ok bool) {
	href, exists := element.Attr("href")
	if exists {
		return conf.Basic.Baseurl + href, true
	}

	return "", false
}

func getNGCDescription(link string) (description string) {

	response, err := http.Get(link)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal("Error loading HTTP response body. ", err)
	}
	document.Find("body>p").Each(func(i int, selection *goquery.Selection) {
		description += selection.Text()
	})
	return description
}

func getImageSrc(ImageLink string) (ImageSrc string) {

	response, err := http.Get(ImageLink)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal("Error loading HTTP response body. ", err)
	}

	image := document.Find("a>img").First()
	src, exists := image.Attr("src")
	if !exists {
		log.Fatal("Image src not found")
	}
	return src
}

func downloadToFile(imageSrc string, ngcNumber string) (filepath string) {
	log.Println("Downloading from " + imageSrc)
	response, e := http.Get(imageSrc)
	if e != nil {
		log.Fatal(e)
	}
	filename := conf.Basic.Imagedir + ngcNumber + conf.Basic.Imageext
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	_, err = io.Copy(file, response.Body)
	if err != nil {
		log.Fatal(err)
	}
	return filename
}

func getNGCNumberFromUserInput() (ngcNumber string) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter NGC Number: ")
	number, _ := reader.ReadString('\n')
	return strings.TrimSpace(number)
}

func init() {
	if _, err := toml.DecodeFile("./config/main.toml", &conf); err != nil {
		log.Fatal(err)
	}
}

func main() {
	DB, err := sql.Open("sqlite3", "ngc.sqlite")
	if err != nil {
		log.Fatal(err)
	}
	defer DB.Close()

	fmt.Println("Welcome to NGCScrapper")

	ngcNumber := getNGCNumberFromUserInput()

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	if _, err := os.Stat(dir + "/" + conf.Basic.Imagedir + ngcNumber + conf.Basic.Imageext); err == nil {
		fmt.Println("File " + ngcNumber + conf.Basic.Imageext + " already exists")
		ngcObject := findNgcObjectByNumber(DB, ngcNumber)
		fmt.Println("===:: NGC #" + ngcObject.Number + " ::===")
		fmt.Println("Filepath: " + ngcObject.Imagepath)
		fmt.Println("Description: " + ngcObject.Description)
		return
	}

	fmt.Println("Search for NGC: " + ngcNumber)

	requestUrl, err := url.Parse(conf.Basic.Baseurl)
	if err != nil {
		log.Fatal(err)
	}
	requestUrl.Path += "ngc.cgi"
	parameters := url.Values{}
	parameters.Add("CatalogNumber", ngcNumber)
	requestUrl.RawQuery = parameters.Encode()

	response, err := http.Get(requestUrl.String())
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal("Error loading HTTP response body. ", err)
	}

	link := document.Find("li>a").First()
	desc := document.Find("li>a").EachWithBreak(func(i int, s *goquery.Selection) bool {
		if i == 1 {
			return true
		}
		return false
	})
	descLink, ok := getNGCDescriptionLink(desc)
	if ok != true {
		log.Fatal("Description link not found")
	}
	ngcDescription := getNGCDescription(descLink)

	imageUrl, ok := getImageLink(link)
	if ok == false {
		log.Fatal("imageUrl link not found")
	}

	src := getImageSrc(imageUrl)
	filepath := downloadToFile(src, ngcNumber)
	log.Println("File " + filepath + " downloaded successfully...")
	newNgcId, err := storeNgcObject(DB, ngcNumber, dir+"/"+filepath, ngcDescription)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("New record added")
	log.Println(newNgcId)
}
